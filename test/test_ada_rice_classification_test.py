#!/usr/bin/env python

"""Tests for `ada_rice_classification_test` package."""

import pytest
from ada_rice_classification_test import constants



@pytest.fixture
def response():
    """Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    # import requests
    # return requests.get('https://github.com/audreyr/cookiecutter-pypackage')


def test_content(response):
    """Sample pytest test function with the pytest fixture as an argument."""
    # from bs4 import BeautifulSoup
    # assert 'GitHub' in BeautifulSoup(response.content).title.string



class TestAda_rice_classification_test:
    """Tests for `ada_rice_classification_test` package."""

    def setup_method(self, test_method):
        """Set up test fixtures, if any."""

    def teardown_method(self, test_method):
        """Tear down test fixtures, if any."""

    def test_000_something(self):
        """Test something."""
