import logging
import mlflow
from mlflow.tracking import MlflowClient


from ada_rice_classification_test import constants

logger = logging.getLogger()
logger.setLevel("INFO")


def train():

    mlflow.set_tracking_uri(constants.MLFLOW_URI)
    exp_name = "Experiment name"
    experiment = MlflowClient().get_experiment_by_name(exp_name)

    if experiment:
        exp_id = experiment.experiment_id
    else:
        exp_id = MlflowClient().create_experiment(
            exp_name, artifact_location=constants.MLFLOW_ARTIFACTS_LOCATION
        )

    with mlflow.start_run(experiment_id=exp_id) as run:

        run_id = run.run_id = run.info.run_uuid
        logging.info(
            f"Starting {exp_name}  experiment with run id:{run_id}, \n\
                        tracking URI: {mlflow.get_tracking_uri()}\n\
                        artifact stored: {mlflow.get_artifact_uri()}"
        )

        mlflow.log_param("run_id", run_id)

        # TODO
        # Put training code here
        # Log parameters, metrics and artifacts with the following commands
        # mlflow.log_param("parameter_name", parameter_value)
        # mlflow.log_params(parameters_dict)
        # mlflow.log_metric("train_loss", train_loss, epoch_number)
        # mlflow.log_artifact(path_to_file)

        # ENDTODO

       

if __name__ == "__main__":
    train()
