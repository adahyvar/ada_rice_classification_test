# -*- coding: utf-8 -*-
"""
Authors        : Kseniia Khakalo
Copyright      : Top Data Science Ltd.
Created        : 23.03.2021
Description    : settings and configurations
"""

__author__ = "Top Data Science Ltd."
from pathlib import Path
import json
import os

# directory paths
CURRENT_DIR = (
    Path(__file__).resolve().parent
)  # Up to "ada_rice_classification_test"/ada_rice_classification_test/
ROOT_DIR = CURRENT_DIR.parents[0]  # Up to ada_rice_classification_test/
DATA_DIR = ROOT_DIR / "data"  # Up to ada_rice_classification_test/data/
MODELS_DIR = ROOT_DIR / "models"  # Up to ada_rice_classification_test/models/
LOGS_DIR = ROOT_DIR / "logs"  # Up to ada_rice_classification_test/logs/
TMP_DIR = ROOT_DIR / "tmp"  # Up to ada_rice_classification_test/tmp/


MLFLOW_DIR = ROOT_DIR / "mlruns"
MLFLOW_URI_LOCAL = f"file://{MLFLOW_DIR}"

# The buckets should be created separately
S3_BUCKET = "s3://ada-rice-classification-test"

MLFLOW_ARTIFACTS_LOCATION = (
    "s3://ada-rice-classification-test-mlflow"
)


# The bellow part will work only after setting up MLFlow service running on EC2 compute instance
# Instructions on how to do that are provided in README
# Other wise mlflow runs will be saved to MLFLOW_URI_LOCAL
credentials_file = ROOT_DIR / "credentials.json"
if credentials_file.exists():
    with open(credentials_file) as f:
        data = json.load(f)
        os.environ["MLFLOW_TRACKING_USERNAME"] = data["MLFLOW_USER"]
        os.environ["MLFLOW_TRACKING_PASSWORD"] = data["MLFLOW_PASSWORD"]

    MLFLOW_URI = f"http://<EC2 IP address>:80"
else:
    MLFLOW_URI = MLFLOW_URI_LOCAL
