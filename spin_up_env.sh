#!/bin/bash
set -e  # Any subsequent(*) commands which fail will cause the shell script to exit immediately

DIRECTORY=.git
if [ ! -d "$DIRECTORY" ]; then
    echo "$DIRECTORY does not exist. Will initialize a GIT repo."
    git init
    echo "$Adding files to GIT"
    git add .
fi


#Create new virtual environment
python3.7 -m venv venv_ada_rice_classification_test

#Activate environment
source venv_ada_rice_classification_test/bin/activate

#Install all requirements from requirements.txt
pip3 install --upgrade pip
pip3 install -r requirements/dev.txt

#Initialize DVC repo
dvc init || true  # continue even if errors were returned 

# Install Git hooks into the DVC repository to automate certain common actions.    
dvc install || true # continue even if errors were returned 

cp git_hooks/pre-commit .git/hooks/pre-commit

dvc remote add -f remote_local /tmp/dvc_remote/ada_rice_classification_test

echo 'SUCCESS'