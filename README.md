# ada_rice_classification_test

Rice classification test project to try out the cvaf 

The structure of the project looks like that:

```bash
    ada_rice_classification_test
    ├── ada_rice_classification_test        # source directory
    │   └── constants.py
    ├── README.md
    ├── data                  # dir where all data should be stored. It can be accessed with constants.DATA_DIR
    ├── git_hooks
    │   └── pre-commit        # custom git hook which aborts git commit if `dvc status` is not up to date
    ├── logs                  # dir where logs should be stored. It can be accessed with constants.LOGS_DIR
    ├── models                # dir where models should be stored. It can be accessed with constants.MODELS_DIR
    ├── setup.cfg             # config with bump2version settings
    ├── setup.py              # more on that bellow
    ├── spin_up_env.sh        # bash script to spin up working environment
    ├── tmp                   # dir for temporary files
    ├── .dvc                  # dir with DVC config files and cache
    └── venv_ada_rice_classification_test   # all files required for virtual environment. Run `source venv_ada_rice_classification_test/bin/activate` to activate
```

## Toolbox

- `virtualenv` for python environment isolation
- `setup.py` for packaging and defining dependencies
- `bump2version` for tracking package version ang tagging when new version is published
- `DVC` for data versioning

## Quickstart

Activate your virtual environment with:

    source venv_ada_rice_classification_test/bin/activate

If the virtual environment was not created yet you can create it with `spin_up_env.sh` bash script.
Please refer to [spin_up_env.sh usage](#spin_up_env.sh-usage) section bellow and return here.

To test if everything works as expected run `pytest`. It should return no errors.

Check what files were added to git with `git status`. Run `git commit -am 'first commit'` if needed.

Now you need to set an origin of the git repo. Create a project in gitlab and run:

    git remote add origin git@gitlab.com:topdatascience/<git_repo_name>

Make sure that your changes came into force with:

    git remote -v

Now push the whole repo to new remote with

    git push

## Setup.py usage

`setup.py` allows you to easily package your code so that it could be used by other python libraries.
To install package with all dependencies inside a virtual environment run:

    (venv_name)$ pip install -e .

### Dependencies

`setup.py` should contain information about all dependencies required to run the code.

The dependencies should be passed to `install_requires` variable. It is equivalent to `requirements.txt` file.

It is also possible specify some extra libraries under `extras_require`. For example: `extras_require={"extra": ["matplotlib>=2.2.0", "jupyter"]}`
Then it can be installed with `pip install -e .[extra]`.

### Package versioning

`setup.py` also contains info about the version of your package.
We use Semantic Versioning, which means MAJOR.MINOR.PATCH convention, where

MAJOR: incompatible API changes

MINOR: add functionality (backwards-compatible)

PATCH: bug fixes (backwards-compatible)

To change the version please use `bump2version` lib.
The behavior of `bump2version` is defined in `setup.cfg` file.
Currently it creates a new git commit and tag when package version is incremented.
To increment the **patch** package version run:

    bump2version patch

If the version was 1.2.3 it will become 1.2.4

## spin_up_env.sh usage

If you haven't spin up the virtual environment when creating the project you can do it now by running

    ./spin_up_env.sh

You might need to `chmod +x spin_up_env.sh` to make the file executable.

The script preforms the following steps:

1. Initiate GIT repo if it doesn't exist and add all files except those listed in `.gitignore` with

        git add .

2. Create virtual environment to isolate python environment for this project from other projects

        python -m venv venv_ada_rice_classification_test
        source venv_ada_rice_classification_test/bin/activate

3. Install required python libs listed in `setup.py` under the `base_requirements` and all extra requirements specified in `extras_require`.
The `-e` flag specifies that we want to install in **editable** mode,
which means that when we edit the files in our package we do not need to re-install the package before the changes come into effect.

        pip install -e .[extra]

4. (Optionally) Set up data version control (DVC)

    Data version control allows to easily track and document changes in the data.
    It improves reproducibility of the experiments because one can map code version with data version.

   1. Initialize dvc repository

        Make sure `.dvc` folder exists in the repo. It means DVC was initialized. Otherwise run `dvc init`

   2. Install Git hooks into the DVC repository to automate certain common actions.

            dvc install

        Here is the list of DVC default hooks:

        - `post-checkout` hook executes `dvc checkout` after `git checkout` to automatically update the workspace with the correct data file versions.
        - `pre-commit` hook executes `dvc status` before git commit to inform the user about the differences between cache and workspace.
        - `pre-push` hook executes `dvc push` before `git push` to upload files and directories tracked by DVC to remote storage.
        If a hook already exists, DVC will raise an exception. In that case, please try to manually edit the existing file or remove it and retry install.

        To abort commit if `dvc status` is not up to date replace `pre-commit` hook with the one from `git_hooks/pre-commit` with

            cp git_hooks/pre-commit .git/hooks/pre-commit

   3. Setup remote_local to tmp folder

            ["remote_local"']
            url = /tmp/dvc_remote/ada_rice_classification_test/constants.py


## S3 bucket for data storage

It is recommended to create two separate S3 buckets for each project.
One for data sharing (s3://ada-rice-classification-test) and another one for storing artifacts saved during MfFlow runs (s3://ada-rice-classification-test-mlflow).


## Set up DVC remote storage

It is highly recommended to set up a **remote** storage for DVC to be able to share and version the data across different machines and users.
DVC works such that hashed copies of the files are stored in the remote storage and when file changes and we run `dvc push` a new version of the file is pushed to the remote. Technically, remote can be an URL to some cloud object storage (S3, Azure blob, GCP Cloud Storage) or a local directory.

To set up a default remote storage run

    dvc remote add -d <remote name> <remote url>

To add a local folder as a default remote:

    dvc remote add remote_local /tmp/dvc_remote/ada_rice_classification_test

To add S3 as default remote:

    dvc remote add -d s3_remote s3://ada_rice_classification_test/data/


To set `tds` as AWS profile to be used with DVC
    
    dvc remote modify s3_remote profile tds

To start tracking some file or directory with DVC run:

    dvc add data/file_or_dir_name

To stop tracking data:

    dvc remove data/file_or_dir_name

To add endpoint URL:

    dvc remote modify s3_remote endpointurl https://object-storage.example.com

To push to a specific remote:

    dvc push -r s3_remote

## Jupyter + GIT

One shouldn't add big notebooks with massive outputs to the git. You can either manually clean outputs in the notebook before 
committing changes to the git or set up an automatic filtering. Commands below show how to do that.
The filter bellow sets "execution_count": null and "outputs": [].

1. Install jq package

    for linux:

        sudo apt-get install jq

    for mac:

        brew install jq

2. Add to `.gitconfig`:

        [filter "clean_ipynb"]
            clean = jq --indent 1 --monochrome-output '. + if .metadata.git.suppress_outputs | not then { cells: [.cells[] | . + if .cell_type == \"code\" then { outputs: [], execution_count: null } else {} end ] } else {} end'
            smudge = cat
        [core]
                attributesfile = PATH/TO/.gitattributes

3. Add to PATH/TO/.gitattributes:

        *.ipynb  filter=clean_ipynb


### Code formatting and linting

We use **black** for code formatting. It will automatically refine your code so that it will comply with PEP8 when you save a file. 
More info on how to set it up with VS Code can be found here: https://marcobelo.medium.com/setting-up-python-black-on-visual-studio-code-5318eba4cd00

We also recommend to use **Pylint** or other linter to highlight syntactical and stylistic problems in your Python source code.
Please have a look how to setup linter in VS Code here: https://code.visualstudio.com/docs/python/linting

# MlFlow

We recommend to set up MlFlow server running on the EC2 AWS instance. It will allow easily share experiment results and artifacts. Alternatively, one can run mlflow locally and store results in `mlruns` local folder. 
To set up ES2 with mlflow running refer to [MLFlow on a remote EC2 server](#mlflow_on_a_remote_ec2_server) instructions. After following the instructions the setup should be the following:

MlFlow server is running on AWS micro `EC2` instance. It runs behind `Nginx` protected by `username` and `password`. The credentials should be specified in `credentials.json` file following the template in [credentials_template.json](credentials_template.json)

MlRun artifacts are stored in the same `S3` bucket where other data is stored in `mlruns` subfolder. It means that to be able to record MLFlow artifacts  `~/.aws/credentials` and  `~/.aws/config` files should be in place.

Mlflow experimants can be accessed from `http://<EC2 public IP>/`.
To access it one would need to provide `username` and `password` .

## MLFlow on a remote EC2 server

First one needs to launch an EC2 instance where MLFlow server will be running.
Compute instance type : `Ubuntu Server 20.04 LTS (HVM), SSD Volume Type` ( `t2.micro` )

Instructions from Hieu:
    When creating ec2 you should choose **tds_vpc** network, use any **public** subnet and security group **demo_sg-202000225...**
    this **demo_sg** has ports: 80, 443 and 8080 open


To be able to access artifacts stored in S3 bucket from MlFlow UI one needs to grand additional permissions for the EC2 machine.
To do that one needs to add `get_models`  IAM role to this EC2 instance (TODO: IAM role will be changed soon). If you don't have enough rights for that, ask admin.


A way to run `mlflow` behind `nginx` is described here: https://medium.com/analytics-vidhya/setup-mlflow-on-aws-ec2-94b8e473618f. Bellow is the list of the steps I followed

1. Install python3

        sudo apt update & sudo apt install python3-pip

2. install MLFlow

        pip3 install mlflow

3. install nginx and open the config file

        sudo apt install nginx

4. install apache2-utils for password protection to the dashboard

        sudo apt install apache2-utils

5. add password for `testuser`

        sudo htpasswd -c /etc/nginx/.htpasswd tds

6. Configure `nginx` to reverse proxy to port 5000

Add the following:

        location / {
        proxy_pass http://localhost:5000/;
        auth_basic “Restricted Content”;
        auth_basic_user_file /etc/nginx/.htpasswd;
        }

to the `/etc/nginx/sites-available/default` file.

        sudo nano /etc/nginx/sites-available/default

The result `/etc/nginx/sites-available/default` file looked like this:

```bash
##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# https://www.nginx.com/resources/wiki/start/
# https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/
# https://wiki.debian.org/Nginx/DirectoryStructure
#
# In most cases, administrators will remove this file from sites-enabled/ and
# leave it as reference inside of sites-available where it will continue to be
# updated by the nginx packaging team.
#
# This file will automatically load configuration files provided by other
# applications, such as Drupal or Wordpress. These applications will be made
# available underneath a path with that package name, such as /drupal8.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

# Default server configuration
#
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        # SSL configuration
        #
        # listen 443 ssl default_server;
        # listen [::]:443 ssl default_server;
        #
        # Note: You should disable gzip for SSL traffic.
        # See: https://bugs.debian.org/773332
        #
        # Read up on ssl_ciphers to ensure a secure configuration.
        # See: https://bugs.debian.org/765782
        #
        # Self signed certs generated by the ssl-cert package
        # Don't use them in a production server!
        #
        # include snippets/snakeoil.conf;

        root /var/www/html;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;


        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                proxy_pass http://localhost:5000/;
                auth_basic "Restricted Content";
                auth_basic_user_file /etc/nginx/.htpasswd;
        }

        # pass PHP scripts to FastCGI server
        #
        #location ~ \.php$ {
        #       include snippets/fastcgi-php.conf;
        #
        #       # With php-fpm (or other unix sockets):
        #       fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
        #       # With php-cgi (or other tcp sockets):
        #       fastcgi_pass 127.0.0.1:9000;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #       deny all;
        #}
}


# Virtual Host configuration for example.com
#
# You can move that to a different file under sites-available/ and symlink that
# to sites-enabled/ to enable it.
#
#server {
#       listen 80;
#       listen [::]:80;
#
#       server_name example.com;
#
#       root /var/www/example.com;
#       index index.html;
#
#       location / {
#               try_files $uri $uri/ =404;
#       }
#}
```

7. Start the `nginx` server and `mlflow` server

        sudo service nginx start
        mlflow server --host 0.0.0.0

If you get `Command 'mlflow' not found`. Make sure that you successfully installed MLflow with `pip show mlflow`. 
If MlFlow was installed try running:

        echo "export PATH=\"`python3 -m site --user-base`/bin:\$PATH\"" >> ~/.bashrc
        source ~/.bashrc


Now you should be able to access the ML server from browser with `http://<IP address>/` the server is protected with login `tds` and password you created on step 5.

If instead of user-password dialog you see NGINX service landing page, try restarting NGINX with `sudo service nginx restart`
