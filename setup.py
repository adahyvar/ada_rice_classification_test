from setuptools import setup, find_packages
from sys import platform

base_requirements = ["scikit-learn"]

platform_requirements = []
if platform == "win32":
    platform_requirements += []
else:
    platform_requirements += []


setup(
    name="ada_rice_classification_test",
    version="0.1.0",
    description="Rice classification test project to try out the cvaf ",
    author="Ada-Maaria Hyvärinen",
    install_requires=(base_requirements + platform_requirements),
    extras_require={"extra": ["matplotlib>=2.2.0", "jupyter"]},
    packages=find_packages(),
    include_package_data=True,
)
